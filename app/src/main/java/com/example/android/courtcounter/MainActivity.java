package com.example.android.courtcounter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int pointA = 0;
    int pointB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * The methods adds the points of each team based on the buttons pressed.
     */
    public void pointATriple(View view) {
        pointA = pointA + 3;
        displayPointsTeamA(pointA);
    }

    public void pointADouble(View view) {
        pointA = pointA + 2;
        displayPointsTeamA(pointA);
    }

    public void pointAFreeThrow(View view) {
        pointA = pointA + 1;
        displayPointsTeamA(pointA);
    }

    public void pointBTriple(View view) {
        pointB = pointB + 3;
        displayPointsTeamB(pointB);
    }

    public void pointBDouble(View view) {
        pointB = pointB + 2;
        displayPointsTeamB(pointB);
    }

    public void pointBFreeThrow(View view) {
        pointB = pointB + 1;
        displayPointsTeamB(pointB);
    }

    public void reset(View view) {
        pointA = 0;
        pointB = 0;
        displayPointsTeamA(pointA);
        displayPointsTeamB(pointB);
    }

    /**
     * These methods display the points for each team.
     */
    public void displayPointsTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }

    public void displayPointsTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }

}
